---
layout: post
title: "Cours suspendus…"
description: "CHER.E.S TOUTES ET TOUS,

SUITE AUX ANNONCES GOUVERNEMENTALES , NOUS SOMMES DANS L’OBLIGATION DE SUSPENDRE LES COURS DE EACS JUSQU’À NOUVEL ORDRE 😥

MERCI DE VOTRE COMPRÉHENSION ET DE VOTRE SOUTIEN.
BIEN À VOUS.
"
picture: 201102.jpg
permalink: /201102

label_default: "actualité"  # grey
label_primary: "cours"  # blue
# label_success: "cours"  # green
# label_warning: "cours"  # orange
# label_danger: "cours"   # red
---

# Cours suspendus…
***
***
CHER.E.S TOUTES ET TOUS,

SUITE AUX ANNONCES GOUVERNEMENTALES , NOUS SOMMES DANS L’OBLIGATION DE SUSPENDRE LES COURS DE EACS JUSQU’À NOUVEL ORDRE 😥

MERCI DE VOTRE COMPRÉHENSION ET DE VOTRE SOUTIEN.

BIEN À VOUS.
<br>
<br>
<img src="../../assets/img/post/201102.jpg" alt="post-photo" width="80%"/>