---
layout: post
title: "La rentrée 2020-2021!"
description: "Bonjour à toutes et tous, nous espérons que vous et votre entourage allez bien…

Les cours de EACS vont reprendre le lundi 1 septembre au gymnase Caillaux, les horaires sont les même. Les cours au gymnase Diderot vont reprendre le mercredi 9 septembre, les horaires n’ai pas changer.

Pensez vous porter le masque! ^_^

Bonne entrée !
"
picture: 200901.jpg
permalink: /200901

label_default: "actualité"  # grey
label_primary: "cours"  # blue
# label_success: "cours"  # green
# label_warning: "cours"  # orange
# label_danger: "cours"   # red

---
# La rentrée 2020-2021!
***
***
Bonjour à toutes et tous, nous espérons que vous et votre entourage allez bien…

Les cours de EACS vont reprendre le lundi 1 septembre au gymnase Caillaux, les horaires sont les même. Les cours au gymnase Diderot vont reprendre le mercredi 9 septembre, les horaires n’ai pas changer.

Pensez vous porter le masque! ^_^

Bonne entrée !
<br>
<br>
<img src="../../assets/img/post/200901.jpg" alt="drawing" width="100%"/>